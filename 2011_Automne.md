# Automne

La jeune fille était assise sur un banc. La rue était déserte. Seuls le vent soufflant dans les branches dénudées et le piaillement de quelques oiseaux se faisaient entendre. Elle contemplait les feuilles orangées jonchant le sol. Couverts de ce tapis glissant et humide à la douce odeur d'humus, les trottoirs étaient voilés par ce patchwork orangé luisant des restes de la rosée matinale qui n'avait encore était asséchée par les rayons de ce soleil automnal. Alice aimait contempler cette scène. Chaque année, elle venait ici, dans son quartier natal, apprécier ce spectacle des plus communs et pourtant si beau et si unique. Chaque année, depuis qu'elle avait 11 ans, elle revenait ici, dès que les premières feuilles tombaient, pour savourer l'odeur des feuilles commençant à se décomposer et écouter la mélodie continue et enivrante du vent. Cette année, la jeune fille avait revêtu un ensemble vert malachite et s'était fait des mèches rouges pour entrer un peu plus en harmonie avec ce qu'elle aimait appeler « son paysage de carte postale ».

De manière imprévue, le souffle du vent cessa de souffler contre les pans de sa jupe courte. Ses longs cheveux vinrent se reposer en douceur sur ses épaules. Elle frémit lorsqu'elle sentit la chaleur d'une main étrangère se poser sur la sienne. Elle se retourna tout en la retirant vivement du banc. A côté d'elle se trouvait une femme d'une trentaine d'années vêtue d'une longue robe blanche à la fois simple et sophistiquée. Sa ligne était soulignée par une ceinture tressée de couleur rouge. Son teint était assez pâle. Ses joues étaient rosies par la température ambiante. Les rayons du soleil se reflétaient dans ses cheveux d'un noir intense. Elle sourit à Alice. Cette dernière lui rendit un sourire à la fois timide et gêné. Qui était-elle ? La jeune fille n’eût pas le temps d’y réfléchir. La dame en blanc la toisait du regard, un regard désireux mais non provocant souligné par un maquillage charbonneux. De manière très calme, elle posa une question.

« Que regardes-tu ici ? »

« Je … J’aime regarder cette … rue à cette période-ci de l’année. Mais pourquoi cette question ? » demanda-t-elle avec un ton hésitant.

« J’aime aussi la contempler.  Un peu comme une artiste contemple son œuvre. »

« Pardon ? » dit Alice un peu surprise.

« Je suis celle qui fait que cette rue soit la plus belle durant l’automne. Chaque feuille est placée comme je le souhaite par le souffle du vent, fixée par la rosée du matin et l’humidité de l’air, séchée par les rayons du soleil.», répondit-elle sans se décontenancer.


La femme avait dit ça avec tant de calme et de certitude, sans même sourciller. Alice était intriguée et ressentait quelque chose d’étrange. Si quelqu’un d’autre lui avait répondu cela, elle se serait inquiétée pour la santé mentale de la personne mais ce n’était pas le cas. Au contraire, de la manière dont l’avait dit la dame en blanc, c’était une évidence. Alice fut de nouveau interrompue dans ses pensées.

« Cela fait maintenant 8 ans que tu reviens à la même période. De mon côté, cela fait autant d’années que je te contemple parcourir cette rue tout en appréciant les subtilités de mon travail. Veux-tu m’accompagner pour en découvrir davantage ? » dit-elle à Alice.

Alice était décontenancée, elle ne savait pas quoi répondre. La femme lui avait dit cela en souriant. Elle se mit à penser qu’elle était dans un rêve, que cette personne en face d’elle était irréelle. Comme pour l’inciter à se décider, la femme tendit sa main à Alice. Sans vraiment réfléchir, répondant à son instinct, Alice saisit la main de la dame en blanc.

« Je m’appelle Olivia. Viens, je vais te montrer ! » annonça-t-elle à Alice avec un sourire radieux.

Les deux femmes firent quelques pas ensemble et disparurent progressivement comme si le brouillard les enveloppait. Jamais plus personne n’eut de nouvelles d’Alice mais certains affirment avoir vu, lorsque les feuilles des arbres commencent à tomber, deux femmes qui s’embrassaient enveloppées d’un étrange brouillard.
