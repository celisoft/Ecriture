# Trois voyageurs, une quête

Un bon fût de bière après l’arrivée des visiteurs, la conversation allait bon train. Elise était émerveillée par leurs aventures vécues dans la cité perdue de Molgorth, les montagnes hantées de l’Olgarth … Le groupe était de passage à l’auberge pour la nuit afin de se ressourcer. Ces derniers étaient à la recherche d’un talisman accessible à des intervalles de temps précis dont un mage avait prédit l’apparition dans la région des siècles auparavant. L’équipe était composée de trois personnes.

Le meneur, Khal, était un homme d’une quarantaine d’années qui avait dû vivre beaucoup plus d’aventure que ses coéquipiers. Il avait les cheveux et le regard d’un noir profond. Ce dernier était d’ailleurs d’autant plus obscur qu’un de ses yeux était traversé par une cicatrice allant jusqu’au menton. Celle-ci avait apparemment été faîte au début de sa carrière d’aventurier par un homme-panthère qui avait fini haché menu par le sabre de Khal.

A sa droite se trouvait Aldren qui n’était autre qu’un ami d’enfance du meneur. Contrairement à Khal, ce dernier était apparemment plutôt spécialisé dans le tir à l’arc et avait un visage plus rassurant que le meneur du groupe.

Pour finir, élément beaucoup plus discret de cette compagnie, une jeune femme d’une trentaine d’année qui ne semblait pas avoir de prédispositions particulières si ce n’est de parler très peu et de beaucoup écouter les autres. Elle ne semblait pas avoir vécu autant d’aventures que ces compagnons : pas de cicatrices, la peau n’avait pas souffert des intempéries. Celle-ci s’appelait Eleonor.

Bien qu’Elise aurait bien voulu continuer à écouter les aventures des trois visiteurs, elle entendit le clapotis de la pluie sur le toit de l’auberge s’accélérer et le tonnerre commençait également à gronder. Elle souhaita donc bonne route au groupe d’aventuriers tout en commençant à mettre son manteau pour affronter les intempéries et se sauva de l’auberge en courant le plus vite possible afin de rejoindre son abris dans la montagne.

Suite au départ précipité de la jeune fille, la compagnie se retrouva sans public et les trois voyageurs demandèrent donc à se reposer chacun dans une chambre. 

