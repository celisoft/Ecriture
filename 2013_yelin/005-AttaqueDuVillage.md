# L'attaque du village

La pluie et le tonnerre continuaient de perturber le calme de la nuit. Les voyageurs étaient si fatigués de leur périple que cela n’allait pas les empêcher de dormir paisiblement … du moins le croyaient-ils…

La chambre d’Eleonor donnait sur le bourg tandis que celles des deux hommes avaient une vue sur la montagne. Bien que par ce temps, la vue n’était pas fort agréable. Tous les trois ne tardèrent pas à se dévêtir et profiter de leur lit et d’un repos bien mérité.

Cependant, au cœur de la nuit, Eleonor se réveilla brutalement. Le médaillon qu’elle portait rougeoyait et était devenu brûlant. Si elle se basait sur ce que la Grande Prêtresse de son ordre lui avait dit avant de la laisser partir du Temple, cela indiquait qu’un dragonacé était présent dans les environs.

Les dragonacés sont une famille d’animaux dont l’un des principaux représentants est le dragon. Ils sont caractérisés par une taille gigantesque, au moins cinq mètres de hauteur pour le plus petit représentant connu, et la capacité à cracher du feu sur des zones plus ou moins grandes. Les dragons ont la faculté de voler dans les airs, ce qui les rends parfois bien utiles pour voyager de montagne en montagne, une fois apprivoisés bien évidemment. Le dragon est d’ailleurs le seul dragonacé qu’il est possible de domestiquer, fait avéré et définitivement validé par l’Académie des Anciens Mages suite à plusieurs accidents causés par des expériences voulant prouver le contraire.

Eleonor bondit dans la chambre de ses compagnons pour les prévenir du danger imminent. Ces derniers furent vraiment surpris par l’irruption de la jeune femme ainsi que par la nouvelle : la région n’était pas réputée pour ses dragons. Ils jetèrent un œil par la fenêtre et comprirent pourquoi : ce n’était pas un dragon qui avait causé la réaction du médaillon mais un mondragon. Ces derniers sont des dragonacés millénaires qui ne se réveillent que très rarement et se reposent pendant des centaines d’années en se repliant sur eux-mêmes formant ainsi des montagnes où la nature se développe.

Immédiatement, ils remirent leur tenue de voyage et donnèrent l’alerte à l’aubergiste et ses locataires. Ils foncèrent dès lors dans les rues afin de se rendre utiles mais ils constatèrent que le chaos régnait déjà : les habitants, complètement paniqués, couraient dans tous les sens. L’aubergiste rejoignit nos aventuriers.

Eleonor fixait du regard le mondragon. Ce dernier avait un regard rougeoyant et regardait en leur direction. Le médaillon dégagea une lumière tellement intense que les gens qui se sauvaient à proximité durent couvrir leur yeux. Aldren comprit cependant que ce n’était pas eux que le monstre observait mais une jeune fille qui courait vers eux. Cette jeune fille n’était autre qu’Elise. Le bijou rougeoyant d’Eleonor se mit à flotter dans les airs et le collier se tendit en direction d’Elise qui désormais tendait la main vers eux comme si sa course allait enfin se finir une fois en leur compagnie. Le mondragon se mit à cracher ses flammes qui cependant ne firent rien à la jeune fille ni au groupe d’aventuriers qu’elle venait de rejoindre. Dans un halo bleuté, le groupe, réunis dans des circonstances moins festives que la veille.

« Le médaillon, il s’est activé, je n’ai rien fait, je ne comprends pas, que se passe-t-il ? », s’exclama Eleonor.

« Je crois que nous avons fini la première étape de notre quête, cette jeune fille est l’héritière descendante d’Ilmarande, donne-lui le médaillon tout de suite avant que nous ne nous fassions tuer par ce monstre infernal ! », lui répondit immédiatement Khal.

Eleonor donna le bijou à la jeune fille qui ne comprenait pas vraiment ce qui se passait. Le mondragon ne comprenait pas non plus pourquoi il ne pouvait pas brûler ce groupe insignifiant et cracha des flammes de nouveau. Aldren banda son arc avec deux flèches. Eleonor prît la main d’Elise pour la poser sur l’épaule de l’archer. Les flèches, auparavant normales, s’enflammèrent de flammes vertes. Aldren visa les yeux du mondragon et décocha ses flèches qui atteignirent toutes deux leur cible. L’animal, privé de sa vue et de sa vie s’écroula.

« Comment en deux flèches avez-vous réussi à tuer une bestiole pareille ?! », s’exclama l’aubergiste.

« Les mondragons ont une grande faiblesse, leurs yeux. La magie distillée par les flammes du médaillon ont accéléré la chose, les flammes vertes sont mortelles pour tout être qui les touche. Du moins, c’est ce que nous avons pu découvrir jusqu’ici … » lui répondit Aldren.

« Il n’a pas été aisé de réunir à la fois le médaillon et une prêtresse du temple de l’Ordre pour pouvoir le découvrir mais je je dois reconnaître que cette jeune fille à un pouvoir latent plus puissant qu’Eleonor pourtant censée être l’une des quatre seules personnes à pouvoir le maîtriser. », ajouta Khal.

« Et qui sont les trois autres ? », s’enquit l’aubergiste.

« La Grande Prêtresse de l’Ordre qui m’a donné le médaillon, le magicien qui l’a fabriqué et qui est mort depuis longtemps et la Mort elle-même. Et cette jeune demoiselle n’est aucune de ces trois personnes. », répondit Eleonor.

Tous les regards se tournèrent vers Elise qui, après de tels événements, ne savait plus trop quoi faire et ne comprenait pas bien ce qu’il venait de se produire.

« On en parlera demain, pour le moment la petite à besoin de repos ! Venez vous aussi, vous êtes mes invités ! », dit l’aubergiste qui prît Elise dans ses bras et l’emmena dans l’auberge qui, par miracle, était encore à moitié debout.
