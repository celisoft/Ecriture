# Un message inattendu

La Porte des Mondes était une des voies uniques pour voyager entre les différents plans du monde et la seule à être empruntée par les dieux eux-mêmes, bien que ces derniers voyageaient peu … Selon le plan d’existence où elle se trouve, son apparence diffère mais dans chacun d’eux elle s’avère être suffisamment gigantesque pour y faire passer une montagne. Celle du monde de Sidh, plan de l’Ombre, était en bois massif, son encadrement était constitué de pierres finement sculptées où arabesques, symboles magiques et écritures antiques se côtoyaient.

Au fur et à mesure que Grald et la Mort approchaient de la Porte, plus le sol vibrait et des bruits sourds tels des coups de canon retentissaient dans la lourde atmosphère de la nuit. Ils stoppèrent net leur avancée lorsqu’ils arrivèrent dans la salle où se trouvait la Porte, figés par le spectacle qui s’offrait à leurs yeux. Les gonds de la Porte, ébranlés par une force mystérieuse, étaient sur le point de céder. Une lumière irisée et bleutée s’infiltrait entre les traverses de cette dernière.

Dans un dernier assaut, la porte céda, laissant place à un invité des plus indésirables : une créature, semblable à un loup, dotée de 4 têtes et mesurant environ six mètres venait de faire irruption dans un halo de lumière bleutée. Craignant pour la sécurité de ses homme, la Mort se concentra immédiatement pour lancer un de ses sorts d’immobilisation dont il avait connaissance. L’étrange animal se figea. Ce dernier s’était calmé soudainement mais la Mort comprit, non sans surprise, que le sort n’avait en rien affecté l’animal mais que celui-ci avait compris l’objectif du sort en lui-même.

- Co … Comment … ? bégaya la Mort.

Dans l’esprit de la Mort, tout se bousculait, nul ne peut résister à un sort lancer par l’un des serviteurs des Dieux si ce n’est un autre serviteur ou les Dieux en personne. Soudainement il chancela, son esprit été abasourdi, oppressé par une puissance psychique incommensurable. Il se concentra, il distingua des bribes de paroles qui se transformèrent bientôt en mots puis en phrases bien distinctes :

- Tu as été l’Elu des Dieux pendant plus de 500 ans, le chant de la Fin a déjà dû arrivé à tes oreilles et pourtant nul n’est encore prêt à recevoir tes responsabilités et ton pouvoir. Mon arrivée en ces lieux signifie que ton descendant est arrivé dans le Monde de Lum et qu’il ne te reste plus qu’à le former. Cependant, bien qu’étant lié à lui, je ne saurai le trouver moi-même. Il t’incombe de le trouver également.

Une fois ces dernières paroles psychiquement prononcées entre l’esprit animal et celui de la Mort, tout en se dissipant, la créature devint rouge incandescent et la Mort se mit à hurler de douleur : le vieil homme de 534 ans venait d’être marqué par le sceau rougeoyant des Enfers. Le signe tant attendu était enfin arrivé, les Enfers réclamaient leur dû, la Mort allait bientôt mourir …

