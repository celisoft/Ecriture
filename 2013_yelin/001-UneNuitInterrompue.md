# Une nuit interrompue

Cela faisait maintenant plusieurs heures que la nuit était tombée sur le monde de Sidh. La plupart des habitants avaient sombré dans un sommeil profond. Repos bien mérité après de si longues heures employées à assister la Mort dans son travail quotidien. Depuis la simple mort de vieillesse au meurtre prémédité en passant par la conservation de la Vie sur Terre, tout est géré ici. Démarches administratives, protection des vivants, destruction des âmes, formation des nouveaux arrivants … la liste des tâches est très longue et nécessite une attention toute particulière sur le respect du calendrier. La nuit était donc la bienvenue pour tout ces travailleurs de l’Ombre parfois soumis à forte pression.

Pendant ce temps, éclairée par la lumière oscillante des flammes, assise dans un fauteuil des plus confortables, la Mort ne dormait point. Cet homme qui avait vécu tant de temps n’arrivait pas à trouver le sommeil. Quelque chose le tracassait, cela faisait maintenant plus d’un mois qu’il avait entendu ce chant. Ce chant que toutes les Morts avaient entendu avant de définitivement quitter le monde de Sidh. Ce chant que seules les âmes en fin de vie peuvent entendre. Il l’avait à la fois attendu et redouté dès qu’il était devenu « celle » que tout les mortels craignent le plus. Mais quand il avait entendu cette douce et belle voix, il s’était dit que Morrigan en personne avait dû chanter cette nuit-là. Bien qu’il savait sa fin proche, il commençait à s’inquiéter de ne pas avoir trouvé de remplaçant. Aucun signe divin ne lui avait permis de trouver celui qui prendrait sa place.

Il entendit les bruits de pas d’un de ses assistants qui s’approchaient pour s’arrêter subitement devant la porte de son logis. L’arrêt des pas battants les pavés du couloir furent suivis de plusieurs coups de battoir sur la porte. Il n’était pas dans les habitudes de ses assistants personnels de le réveiller en pleine nuit même s’ils savaient que leur chef dormait peu. La Mort se leva donc pour ouvrir à la personne qui continuait d’utiliser le battoir sur la porte de bois. Quand il ouvrit cette dernière, il reconnut Grald, le responsable de la sécurité des Mondes. Il était essoufflé, ce qui, pour lui, signifiait qu’il avait dû courir pendant une certaine distance. Et pour que Grald se mette dans l’état de panique où il semblait être, il fallait que ce soit urgent.

- Que se pa …

Grald ne lui laissa pas le temps de finir sa question.

- Seigneur ! La Porte … la Porte des Mondes … il faut que vous … que vous veniez immédiatement !! dit-il en reprenant son souffle.

